import yararacad as yc

def otb(x: yc.dim=3, y: yc.dim=3, z: yc.dim=3, thickness: yc.dim=1) -> yc.Solid:
	return yc.apply_thickness( thickness, yc.Cuboid(x, y, z))\
	.remove_faces(yc.upward_facing)


